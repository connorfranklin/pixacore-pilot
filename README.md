# Pixacore Pilot

## Front-end Best Practices

...

### Images

#### Use `<img>` tags, always.

`<img>` tags have native access to the parameters of the image, and therefore they can automatically scale (when `display:block`) and flow with text (when `display:inline`). These features are crucial for responsive design.

Examples of use cases where `background-image` is appropriate:

+ HTML text or other content is expected to float above a static image

+ That's it.

#### Store images in repository directory, not WP Media Library

This way, everything is managed directly in the repository, and is tracked.

### Positioning and Wrapping

#### Avoid absolute and relative Positioning

Avoid absolute and relative positioning, especially when the position is relative to the page (`<body>` or `<html>` element).

This messes with the flow of the page, and if proliferated, might necessitate the adjusting of multiple additional elements when changing position of another.

Absolute & relative positioning seems to be used often to accomplish multiple layers of graphics in background images. This is the perfect opportunity to use `background-image`. If multiple images need to be floated, at that point it becomes appropriate to use absolute positioned items and `z-index`, but only in the context of (relative to) the container in question.

## WP Best Practices

### Simplify

Despite the frequent requirement that we use WP for development, we have few features that and processes that actually require WP.

Use of Wordpress features and plugins is inconsistent, and results in development inefficiencies. Devs are required to investigate how and where a previous change was made in order to make a fix.

**Below are some basic ways we can avoid such unneccesary confusion and complextiy in the future:**

+ Each unique page should have a dedicated template or partial.

+ These templates and partials are to include all page markup, copy, images, and other editorial features.

+ If JS or CSS plugins are required for user interaction, install via [Bower (package manager for front-end components)](https://bower.io/), and include using [wp_enqueue_script() and wp_enqueue_style()](https://developer.wordpress.org/themes/basics/including-css-javascript/#enqueuing-scripts-and-styles)

+ Use WP Plugins only for features that require back-end support and can only be accomplished within WP (think forms and other user interactions that must report locally). Install via [Composer](https://roots.io/using-composer-with-wordpress/).

+ Use a static images directory rather than the WP Gallery.

**With these standards in place, we should enjoy the following benefits:**

+ Editorial changes are centralized. No more searching WP Posts, Pages, Custom Fields, Templates and more to determine where a text node or image originates.

+ Editorial Changes, CSS Changes, Image Changes etc. are all managed identically, with the full advantages of Git

+ DB imports are seldom required for routine changes.
### Bedrock

[Bedrock](https://roots.io/bedrock/) is part of the [Roots](https://roots.io/) suite of WP Boilerplate tools. Its features include:

+ Isolation of configurations for development, staging, and production environments

+ Security best practices are baked in

+ Composer is installed by default

#### Security details for nerds:

+ *In order not to expose sensetive files in the webroot, Bedrock moves what's required into a web/ directory including the vendor'd wp/ source, and the wp-content source.*

+ *wp-content has been named app to better reflect its contents. It contains application code and not just "static content". It also matches up with other frameworks*

+ *wp-config.php remains in the web/ because it's required by WP, but it only acts as a loader. The actual configuration files have been moved to config/ for better separation.*

+ *vendor/ is where the Composer managed dependencies are installed to.*

+ *wp/ is where the WordPress core lives. It's also managed by Composer but can't be put under vendor due to WP limitations.*
*(Bedrock is intended to run on top of Roots/Trellis as part of a LEMP stack. For the time being, we'll continue use our existing LAMP stack for client and TeamSite support.)*

### WP Unit Testing

[WP Unit Tests](http://wptest.io/about/)

## VMs (with Vagrant and Ansible)

By using VMs for local development, we'll factor out the issues inherent with our current local dev environments. At the moment, some of us develop directly in MacOS, or Windows, using different configurations of XAMP and native Apache and PHP. This can create unneeded complexities, especially when it comes time to hand off a project to a new dev, or integrate our DB into a new environment.

## Task-specific Branches

To keep our tasks separate and organized during development, let's begin starting a new branch for each open issue. This will be automated via JIRA.

### JIRA Integration

...

### Branching and Merging

...

### Merges to Master Branch Result in Deployment to Dev Server

...
